Installation / Configuration
----------------------------

To install Delight by HBS follow the steps as below.

1. Get the latest copy from https://gitlab.com/arunanson/delight-isp-config-theme/repository/archive.zip?ref=master
2. Extract the archive using any compatible tool like winrar
3. Make a copy of 'default' theme. (For backup)
4. upload the contents of folder 'delight-isp-config-theme' 
to /usr/local/ispconfig/interface/web/themes/default/ folder.

If your theme hasn't changed yet,
4. Login to your ISP Config installation as administrator.
5. Click on 'Tools' from the main header menu.
6. Go to 'Interface' from the left side menu.
7. Select 'default' against the 'Design' setting.
8. Hit 'save' button.
9. Thank us by passing an email to hello(at)helloinfinity.com telling how we helped you. :)